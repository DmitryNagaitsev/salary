import React, { useState, useRef } from 'react';
import SwitchControl from '../switch-control';
import { Field, reduxForm, formValueSelector, change } from 'redux-form';
import { Overlay, Tooltip } from 'react-bootstrap';
import './salary-form.scss';
import { connect } from 'react-redux';

const SalaryFormTemplate = (props: any) => {
  const { salary, sum, withoutTax, dispatch } = props;
  const [showTooltip, setShowTooltip] = useState(false);
  const target = useRef(null);

  const period = () => {
    switch (sum) {
      case 'daily':
        return ' в день';
      case 'hourly':
        return ' в час';
      default:
        return '';
    }
  };

  const tooltip = (
    <div className="tooltip-container">
      <i
        className="mdi mdi-close-circle-outline ml-2"
        onClick={() => setShowTooltip(false)}
        ref={target}
      ></i>
      <Overlay target={target} show={showTooltip} placement="bottom-start">
        {props => (
          <Tooltip
            id="salary-form-minimal-tooltip"
            className="salary-minimal-tooltip"
            {...props}
          >
            МРОТ - минимальный размер оплаты труда. Разный для разных регионов.
          </Tooltip>
        )}
      </Overlay>
    </div>
  );

  const salaryBlock = (
    <div className="ml-2 pl-4">
      <div>
        <Field name="withoutTax" component={SwitchControl} />
      </div>
      <div className="d-flex">
        <Field
          name="salary"
          component="input"
          type="text"
          className="salary-input form-control"
        />{' '}
        <label className="ml-2">&#8381; {period()}</label>
      </div>
    </div>
  );

  const salaryInfo = () => {
    const companyPayment = Math.floor(withoutTax ? +salary / 0.87 : +salary);
    const employeeIncome = withoutTax
      ? +salary
      : Math.floor(companyPayment * 0.87);
    const tax = companyPayment - employeeIncome;

    return (
      <div className="salary-info-container mt-5">
        <div>{employeeIncome} &#8381; сотрудник будет получать на руки</div>
        <div>{tax} &#8381; НДФЛ, 13% от оклада</div>
        <div>{companyPayment} &#8381; за сотрудника в месяц</div>
      </div>
    );
  };

  const clearSalary = () => {
    dispatch(change('salary', 'withoutTax', true));
    dispatch(change('salary', 'salary', ''));
  };

  return (
    <>
      <div className="salary-form-container">
        <form>
          <div>
            <label className="form-label">Сумма</label>
            <div className="sum-container pl-2">
              <div className="form-check custom-control custom-radio d-flex align-items-center">
                <Field
                  name="sum"
                  className="form-check-input custom-control-input mt-0"
                  component="input"
                  type="radio"
                  value="monthly"
                  id="salary-form-sum-monthly"
                  onChange={() => clearSalary()}
                />
                <label
                  className="form-check-label custom-control-label font-weight-bold"
                  htmlFor="salary-form-sum-monthly"
                >
                  Оклад за месяц
                </label>
              </div>
              <div className="form-check custom-control custom-radio d-flex align-items-center">
                <Field
                  name="sum"
                  className="form-check-input custom-control-input mt-0"
                  component="input"
                  type="radio"
                  value="minimal"
                  id="salary-form-sum-minimal"
                  onChange={() => clearSalary()}
                />
                <label
                  className="form-check-label custom-control-label font-weight-bold"
                  htmlFor="salary-form-sum-minimal"
                >
                  МРОТ
                </label>
                {!showTooltip && (
                  <i
                    className="mdi mdi-information-outline ml-2"
                    onClick={() => setShowTooltip(true)}
                  ></i>
                )}
                {showTooltip && tooltip}
              </div>
              <div className="form-check custom-control custom-radio d-flex align-items-center">
                <Field
                  name="sum"
                  className="form-check-input custom-control-input mt-0"
                  component="input"
                  type="radio"
                  value="daily"
                  id="salary-form-sum-daily"
                  onChange={() => clearSalary()}
                />
                <label
                  className="form-check-label custom-control-label font-weight-bold"
                  htmlFor="salary-form-sum-daily"
                >
                  Оплата за день
                </label>
              </div>
              <div className="form-check custom-control custom-radio d-flex align-items-center">
                <Field
                  name="sum"
                  className="form-check-input custom-control-input mt-0"
                  component="input"
                  type="radio"
                  value="hourly"
                  id="salary-form-sum-hourly"
                  onChange={() => clearSalary()}
                />
                <label
                  className="form-check-label custom-control-label font-weight-bold"
                  htmlFor="salary-form-sum-hourly"
                >
                  Оплата за час
                </label>
              </div>
            </div>
          </div>
          {sum !== 'minimal' && salaryBlock}
        </form>
      </div>
      {salary && sum === 'monthly' && salaryInfo()}
    </>
  );
};

const selector = formValueSelector('salary');
const SalaryForm = connect(state => ({
  salary: selector(state, 'salary'),
  sum: selector(state, 'sum'),
  withoutTax: selector(state, 'withoutTax'),
  initialValues: { sum: 'monthly', withoutTax: true }
}))(
  reduxForm({
    form: 'salary'
  })(SalaryFormTemplate)
);

export default SalaryForm;
