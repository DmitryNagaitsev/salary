import React from 'react';
import './switch-control.scss';

const SwitchControl = (props: {
  input: { onChange: (value: boolean) => any; value: boolean };
}) => {
  const {
    input: { onChange, value }
  } = props;

  return (
    <div className="switch-control-container">
      <div className="custom-control custom-switch">
        <input
          type="checkbox"
          className="custom-control-input"
          id="salarySwitch"
          checked={value}
          onChange={event => onChange(event.target.checked)}
        />
        <label className="left-label" htmlFor="salarySwitch">
          Указать с НДФЛ
        </label>
        <label className="custom-control-label" htmlFor="salarySwitch"></label>
        <label className="right-label" htmlFor="salarySwitch">
          Без НДФЛ
        </label>
      </div>
    </div>
  );
};

export default SwitchControl;
